package ex2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;

public class InterfataGrafica extends JFrame{
    private String fileName;
    private JButton buton;
    private JTextArea t;


    public InterfataGrafica(){
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(400, 400);
        setVisible(true);
        setLayout(null);

        init();
    }

    class TratareButon implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            try {
                File fileName = new File("file.txt");
                FileWriter fileWriter = new FileWriter(fileName);

                String text = t.getText();

                fileWriter.write(text);
                fileWriter.close();

            } catch (Exception exception) {
                exception.printStackTrace();
            }
        }
    }

    public void init() {

        int width = 80;
        int height = 30;
        this.fileName =  "file.txt";
        this.buton = new JButton("Buton");
        this.t = new JTextArea();


        t.setBounds(70, 10, width, height);

        buton.setBounds(70, 80, width, height);
        buton.addActionListener(new TratareButon());

        add(t);
        add(buton);



    }

    public static void main(String[] args) {
        InterfataGrafica i = new InterfataGrafica();
    }
}
