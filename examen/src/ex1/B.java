package ex1;

public class B extends A{
    private String param;
    private C c;
    private D d;

    public B(){

    }
    public B(String p){
        super();
        this.param = p;
    }

    public String getParam() {
        return param;
    }

    public void setParam(String param) {
        this.param = param;
    }
}
